/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ PowerManager (https://gitlab.com/DesQ/DesQUtils/PowerManager)
 * DesQ PowerManager manager power savings settings.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "Global.hpp"

namespace DFL {
    namespace Power {
        class Manager;
    }
}

namespace DesQ {
    namespace Power {
        class Manager;
        class TrayIcon;

        typedef struct battery_state_t {
            bool      discharging = false;
            double    charge      = 0;
            qlonglong timeOut     = 0;

            bool      lowNoticeShown  = false;
            bool      critNoticeShown = false;
        } BatteryState;
    }
}

class DesQ::Power::Manager : public QWidget {
    Q_OBJECT

    public:
        Manager();
        ~Manager();

        /** Refresh the battery state */
        void refresh();

        /** Show the tray icon */
        void showTrayIcon();

    private:
        void notifyUser( QString title, QString message, short weight = 1 );

        void loadPowerProfiles();

        DFL::Power::Manager *dflPower;
        TrayIcon *tray = nullptr;

        const QString mID = "77";

        uint mLowPercentage      = 15;
        uint mCriticalPercentage = 2;

        BatteryState mBattState;

        void handleBatteryChargeChanged();

        /** Handle the tray's activated(...) signal */
        void handleTrayActivation( QSystemTrayIcon::ActivationReason );

    Q_SIGNALS:
        void chargeChanged( double, qlonglong, bool );
};
