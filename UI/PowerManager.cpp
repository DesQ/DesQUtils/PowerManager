/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ PowerManager (https://gitlab.com/DesQ/DesQUtils/PowerManager)
 * DesQ PowerManager manager power savings settings.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "PowerManager.hpp"
#include "PowerManagerTray.hpp"

#include <desq/desq-config.h>
#include <DFSettings.hpp>
#include <DFPowerManager.hpp>


DesQ::Power::Manager::Manager() {
    /** Backend */
    dflPower = new DFL::Power::Manager( this );

    /** Tray Icon */
    tray = new DesQ::Power::TrayIcon( this );

    /** Battery percentages */
    mLowPercentage      = (uint)powerSett->value( "LowBattery" );
    mCriticalPercentage = (uint)powerSett->value( "CriticalBattery" );

    dflPower->setBatteryChargeLevel( DFL::Power::LowCharge,      mLowPercentage );
    dflPower->setBatteryChargeLevel( DFL::Power::CriticalCharge, mCriticalPercentage );

    connect( dflPower, &DFL::Power::Manager::switchedToACPower,    this, &DesQ::Power::Manager::handleBatteryChargeChanged );
    connect( dflPower, &DFL::Power::Manager::switchedToBattery,    this, &DesQ::Power::Manager::handleBatteryChargeChanged );
    connect( dflPower, &DFL::Power::Manager::batteryChargeChanged, this, &DesQ::Power::Manager::handleBatteryChargeChanged );

    /** Handle activation */
    connect( tray,     &QSystemTrayIcon::activated,                this, &DesQ::Power::Manager::handleTrayActivation );

    /** Handle tray signals */
    connect(
        tray, &DesQ::Power::TrayIcon::toggleVisible, [ = ] () {
            if ( isVisible() ) {
                hide();
            }

            else {
                show();
            }
        }
    );

    connect(
        tray, &DesQ::Power::TrayIcon::quit, [ = ] () {
            qApp->quit();
        }
    );

    /** Update the icon */
    connect( this, &DesQ::Power::Manager::chargeChanged, tray, &DesQ::Power::TrayIcon::updateIcon );

    QHash<int, QString> profiles;

    profiles[ DFL::Power::OnAcPower ]         = "OnAcPower";
    profiles[ DFL::Power::OnBattery ]         = "OnBattery";
    profiles[ DFL::Power::OnLowBattery ]      = "OnLowBattery";
    profiles[ DFL::Power::OnCriticalBattery ] = "OnCriticalBattery";

    for ( int id: profiles.keys() ) {
        DFL::Power::PowerStateConfig cfg;
        cfg.Action       = (int)powerSett->value( profiles[ id ] + "::Action" );
        cfg.Script       = (QString)powerSett->value( profiles[ id ] + "::Script" );
        cfg.Brightness   = (double)powerSett->value( profiles[ id ] + "::Brightness" );
        cfg.IdleTimeOut  = (int)powerSett->value( profiles[ id ] + "::IdleTimeOut" );
        cfg.OnIdle       = (int)powerSett->value( profiles[ id ] + "::OnIdle" );
        cfg.OnResume     = (int)powerSett->value( profiles[ id ] + "::OnResume" );
        cfg.IdleScript   = (QString)powerSett->value( profiles[ id ] + "::IdleScript" );
        cfg.ResumeScript = (QString)powerSett->value( profiles[ id ] + "::ResumeScript" );

        cfg.LidConfigs[ DFL::Power::LidOpen ]        = DFL::Power::LidStateConfig();
        cfg.LidConfigs[ DFL::Power::LidOpen ].Action = (int)powerSett->value( profiles[ id ] + "::LidOpened" );
        cfg.LidConfigs[ DFL::Power::LidOpen ].Script = (QString)powerSett->value( profiles[ id ] + "::OnOpenScript" );

        cfg.LidConfigs[ DFL::Power::LidClosed ]        = DFL::Power::LidStateConfig();
        cfg.LidConfigs[ DFL::Power::LidClosed ].Action = (int)powerSett->value( profiles[ id ] + "::LidClosed" );
        cfg.LidConfigs[ DFL::Power::LidClosed ].Script = (QString)powerSett->value( profiles[ id ] + "::OnCloseScript" );

        dflPower->addPowerConfiguration( (DFL::Power::PowerState)id, cfg );
    }

    dflPower->startManagement();

    /** For testing purposes */
    // double pc     = 0;
    // QTimer *timer = new QTimer();
    // timer->start( 100 );
    // connect(
    //     timer, &QTimer::timeout, [ = ]() mutable {
    //         handleBatteryChargeChanged( pc );
    //         pc += 0.5;
    //
    //         if ( pc >= 100.0 ) {
    //             timer->stop();
    //         }
    //     }
    // );
}


DesQ::Power::Manager::~Manager() {
    delete dflPower;
}


void DesQ::Power::Manager::refresh() {
    DFL::Power::PowerStatus sts = dflPower->powerStatus();

    mBattState.discharging = ( sts.powerState != DFL::Power::OnAcPower );
    mBattState.charge      = sts.batteryCharge;
    mBattState.timeOut     = ( mBattState.discharging ? sts.timeToEmpty : sts.timeToFull );

    emit chargeChanged( mBattState.charge, mBattState.timeOut, !mBattState.discharging );
}


void DesQ::Power::Manager::showTrayIcon() {
    refresh();
    tray->show();
}


void DesQ::Power::Manager::notifyUser( QString title, QString message, short weight ) {
    /** Always show critical notifications */
    if ( ( (bool)powerSett->value( "ShowNotifications" ) == false ) && ( weight != 2 ) ) {
        return;
    }

    QString urgency;

    switch ( weight ) {
        case 0: {
            urgency = "low";
            break;
        }

        case 1: {
            urgency = "normal";
            break;
        }

        default: {
            urgency = "critical";
            break;
        }
    }

    QProcess::startDetached(
        "desq-notifier", {
            "-i", "desq", "-a", "desq-power-manager",
            "-t", "5000", "-u", urgency, "-r", mID,
            title, message
        }
    );
}


/* Battery charge changed */
void DesQ::Power::Manager::handleBatteryChargeChanged() {
    /**
     * Sometimes, it may take upto a second to get proper battery stats
     * We will introduce a synthetic delay of 1 second. This will prevent
     * spurious critical notifications.
     */
    int t = 0;

    do {
        QThread::msleep( 10 );
        t += 10;

        qApp->processEvents();
    } while ( t < 1000 );

    DFL::Power::PowerStatus sts = dflPower->powerStatus();

    mBattState.charge = sts.batteryCharge;

    qDebug() << "Battery charge changed.";
    qDebug() << "  Charge:" << sts.batteryCharge;
    qDebug() << "  On AC :" << ( sts.powerState == DFL::Power::OnAcPower ? true : false ) << mBattState.discharging;
    qDebug() << "  ETR   :" << sts.timeToEmpty;
    qDebug() << "  ETF   :" << sts.timeToFull;

    if ( sts.powerState == DFL::Power::OnAcPower ) {
        /** If the low/critical notice was shown, close it */
        if ( mBattState.lowNoticeShown || mBattState.critNoticeShown ) {
            mBattState.lowNoticeShown  = false;
            mBattState.critNoticeShown = false;
            QProcess::startDetached( "desq-notifier", { "-x", mID } );
        }

        /** Sleep 10 ms */
        QThread::msleep( 10 );

        /** Previously it was on battery */
        if ( mBattState.discharging ) {
            notifyUser(
                "AC Connected",
                "The AC Power supply has been connected. Your battery is charging."
            );

            mBattState.discharging = false;
        }
    }

    else {
        /** Critical state */
        if ( ( sts.timeToEmpty <= 60 ) and ( sts.timeToEmpty != 0 ) ) {
            qDebug() << "Battery Empty";

            /** The notice was not shown to the user */
            if ( mBattState.critNoticeShown == false ) {
                mBattState.critNoticeShown = true;

                notifyUser(
                    "Battery Empty",
                    "Your battery is has under 1 minute of charge is remaining. Save your work immediately. "
                    "Connect a charger or find an alternate source of power supply to continue working.",
                    2
                );
            }
        }

        else if ( ( sts.timeToEmpty <= 300 ) and ( sts.timeToEmpty != 0 ) ) {
            qDebug() << "Battery nearly empty";

            /** The notice was not shown to the user */
            if ( mBattState.critNoticeShown == false ) {
                mBattState.critNoticeShown = true;

                notifyUser(
                    "Battery Nearly Empty",
                    "Your battery has under 5 minutes of charge is remaining. Save your work immediately. "
                    "Connect a charger or find an alternate source of power supply to continue working.",
                    2
                );
            }
        }

        else if ( mBattState.charge <= mCriticalPercentage ) {
            qDebug() << "Critical battery";

            /** The notice was not shown to the user */
            if ( mBattState.critNoticeShown == false ) {
                mBattState.critNoticeShown = true;

                notifyUser(
                    "Battery Critical",
                    QString( "Your battery has less than %1% charge remaining. Save your work immediately. "
                             "Connect a charger or find an alternate source of power supply to continue working." ).arg( mCriticalPercentage ),
                    2
                );
            }
        }

        else if ( mBattState.charge <= mLowPercentage ) {
            qDebug() << "Low battery";

            /** The notice was not shown to the user */
            if ( mBattState.lowNoticeShown == false ) {
                mBattState.lowNoticeShown = true;

                notifyUser(
                    "Battery Low",
                    QString( "Your battery has less than %1% charge remaining. "
                             "Keep a charger or an alternate source of power supply ready." ).arg( mLowPercentage ),
                    2
                );
            }
        }

        else {
            /** If a notification is shown, close it */
            if ( mBattState.lowNoticeShown || mBattState.critNoticeShown ) {
                mBattState.lowNoticeShown  = false;
                mBattState.critNoticeShown = false;
                QProcess::startDetached( "desq-notifier", { "-x", mID } );
            }

            /** Sleep 10 ms */
            QThread::msleep( 10 );

            /** We were previously on On AC Power */
            if ( mBattState.discharging == false ) {
                notifyUser(
                    "AC Disconnected",
                    "The AC Power supply has been disconnected. You're running on battery backup."
                );

                mBattState.discharging = true;
            }
        }
    }

    /** Finally, update the battery icon */
    emit chargeChanged( mBattState.charge, mBattState.timeOut, !mBattState.discharging );
}


void DesQ::Power::Manager::handleTrayActivation( QSystemTrayIcon::ActivationReason reason ) {
    switch ( reason ) {
        case QSystemTrayIcon::Trigger: {
            qDebug() << "Triggered";

            if ( isVisible() ) {
                qDebug() << "Hiding";
                hide();
            }

            else {
                qDebug() << "Showing";
                show();
            }

            break;
        };

        case QSystemTrayIcon::MiddleClick: {
            qDebug() << "Middle-click";
            qApp->quit();
            break;
        };

        case QSystemTrayIcon::DoubleClick:
        case QSystemTrayIcon::Context: {
            break;
        };

        default: {
            break;
        };
    }
}
