/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ PowerManager (https://gitlab.com/DesQ/DesQUtils/PowerManager)
 * DesQ PowerManager manager power savings settings.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "PowerManager.hpp"
#include "PowerManagerTray.hpp"

DesQ::Power::TrayIcon::TrayIcon( QWidget *parent ) : QSystemTrayIcon( parent ) {
    setIcon( QIcon( ":/icons/desq-power-manager.png" ) );

    QMenu *menu = new QMenu( "TrayMenu" );

    menu->addAction( QIcon( ":/icons/desq-power-manager.png" ), "Show PowerManager",  this, &DesQ::Power::TrayIcon::toggleVisible );
    menu->addAction( QIcon::fromTheme( "application-exit" ),    "&Quit PowerManager", this, &DesQ::Power::TrayIcon::quit );
    setContextMenu( menu );
}


void DesQ::Power::TrayIcon::updateIcon( double value, qlonglong timeOut, bool onAC ) {
    /**
     * value -> Battery Percentage
     * onAC  -> AC Power Connected
     */

    QString toolTipText = "DesQ Power Manager<br>";

    if ( onAC ) {
        if ( qFuzzyCompare( value, 100 ) ) {
            toolTipText += "Fully charged";
        }

        else {
            toolTipText += QString( "%1 % (charging)" ).arg( value, 0, 'f', 0 );
        }
    }

    else {
        toolTipText += QString( "%1 % (discharging)" ).arg( value, 0, 'f', 0 );
    }

    toolTipText += QString( "<br>%1 %2 remaining." ).arg(
        ( timeOut >= 3600 ? timeOut / 3600.0 : ( timeOut >= 60 ? ceil( timeOut / 60.0 ) : timeOut ) ), 0, 'f', ( timeOut >= 3600 ? 1 : 0 )
    ).arg(
        ( timeOut >= 3600 ? "h" : ( timeOut >= 60 ? "m" : "s" ) )
    );

    setToolTip( toolTipText );

    if ( onAC and value > 99.0 ) {
        setIcon( QIcon( ":/icons/desq-power-manager.png" ) );
        return;
    }

    QColor border;

    double mFraction = value / 100.0;
    int    red = 0, green = 0;

    // Critical: Bright Red
    if ( value < 5.0 ) {
        // Bright Red
        red   = 0xff;
        green = 0x00;
    }

    // Change from Red to Yellow: When mFraction = 0.05, green = 0; mFraction = 0.4, green = 0xBB
    else if ( mFraction < 0.4 ) {
        red   = 0xBB;
        green = ( int )( ( mFraction - 0.05 ) * 550 );
    }

    // Remain Yellow
    else if ( mFraction <= 0.6 ) {
        red   = 0xBB;
        green = 0xBB;
    }

    // Change from Yellow to Green: When totalF = 0.6, red = 0xBB; mFraction = 1, red = 0;
    else {
        red   = ( int )( ( 1 - mFraction ) * 637.5 );
        green = 0xBB;
    }

    border = QColor( red, green, 0 );

    /* Battery charge gradient */
    QLinearGradient gradient( QPoint( 100, 0 ), QPoint( 220, 0 ) );

    gradient.setColorAt( 0.0, border.darker( 120 ) );
    gradient.setColorAt( 0.4, border );
    gradient.setColorAt( 0.6, border );
    gradient.setColorAt( 1.0, border.darker( 120 ) );
    QBrush fill = QBrush( gradient );

    /* Clean the image */
    QImage   img( 320, 320, QImage::Format_ARGB32 );
    QPainter p( &img );

    p.setRenderHints( QPainter::Antialiasing );
    img.fill( Qt::transparent );

    /* Border */
    p.save();
    p.setPen( QPen( border, 10, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin ) );
    p.drawRoundedRect( QRect( 90, 30, 140, 280 ), 10, 10 );
    p.setPen( Qt::NoPen );
    p.setBrush( border );
    p.drawRoundedRect( QRect( 120, 5, 80, 30 ), 10, 10 );
    p.restore();

    /* Charge of the battery */
    p.save();
    p.setPen( Qt::NoPen );
    p.setBrush( fill );
    p.drawRoundedRect( QRect( 100, 40 + 260 * ( 100 - value ) / 100.0, 120, 260 * value / 100.0 ), 5, 5 );
    p.restore();

    if ( onAC ) {
        QPainterPath pp;
        pp.moveTo( QPoint( 200, 60 ) );
        pp.lineTo( QPoint( 120, 170 ) );
        pp.lineTo( QPoint( 200, 170 ) );
        pp.lineTo( QPoint( 120, 280 ) );

        p.save();
        p.setPen( QPen( QColor( "#CCCC00" ), 20, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin ) );
        p.setBrush( QColor( "#AABBBB00" ) );
        p.drawPath( pp );
        p.restore();
    }

    else if ( value < 5 and not onAC ) {
        /** Draw a red exclamation point */
        p.save();
        p.setPen( QPen( QColor( Qt::darkRed ), 30, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin ) );
        p.drawLine( QPointF( 160, 60 ),  QPointF( 160, 200 ) );
        p.drawLine( QPointF( 160, 240 ), QPointF( 160, 250 ) );
        p.restore();
    }

    p.end();

    setIcon( QIcon( QPixmap::fromImage( img ) ) );
}
