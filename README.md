# DesQ Power Manager
Power Manager is the executable that sits in the tray informs the user about the power status.
It also performs some background tasks, like perform predefined action on idle, lid close etc.


### Dependencies:
* Qt5 (qtbase5-dev, qtbase5-dev-tools)
* UPower (upower)
* Systemd/elogind
* LibDesQ (https://gitlab.com/DesQ/libdesq.git)
* LibDesQUI (https://gitlab.com/DesQ/libdesqui.git)
* WayQt (https://gitlab.com/desktop-frameworks/wayqt.git)
* DFL::Power (https://gitlab.com/desktop-frameworks/power.git)
* DFL::Settings (https://gitlab.com/desktop-frameworks/settings.git)
* DFL::GammaEffects (https://gitlab.com/desktop-frameworks/gamma-effects.git)


### Notes for compiling (Qt5) - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/DesQ/DesQUtils/PowerManager.git DesQPowerManager`
- Enter the `DesQPowerManager` folder
  * `cd DesQPowerManager`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Known Bugs
* Please test and let me know


### Upcoming
* Battery usage chart (when supported by the hardware)
* Multiple idle timeouts
