/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ PowerManager (https://gitlab.com/DesQ/DesQUtils/PowerManager)
 * DesQ PowerManager manager power savings settings.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

// Local Headers
#include "PowerManager.hpp"
#include "PowerManagerTray.hpp"

#include <signal.h>

#include <desq/Utils.hpp>

#include <DFUtils.hpp>
#include <DFSettings.hpp>
#include <DFApplication.hpp>
#include <DFXdg.hpp>

DFL::Settings *powerSett = nullptr;

int main( int argc, char *argv[] ) {
    QDir cache( DFL::XDG::xdgCacheHome() );

    DFL::log = fopen( cache.filePath( "DesQ/Power.log" ).toLocal8Bit().data(), "a" );
    qInstallMessageHandler( DFL::Logger );

    qDebug() << "------------------------------------------------------------------------";
    qDebug() << "DesQ Power Manager started at" << QDateTime::currentDateTime().toString( "yyyyMMddThhmmss" ).toUtf8().constData();
    qDebug() << "------------------------------------------------------------------------";

    DFL::Application app( argc, argv );

    app.setOrganizationName( "DesQ" );
    app.setApplicationName( "Power" );
    app.setApplicationVersion( PROJECT_VERSION );

    /** This is a hack to set proper app ID: desq-powermanager */
    app.setDesktopFileName( "desq-power" );

    QObject::connect(
        &app, &DFL::Application::unixSignalReceived, [ &app ] ( int signum ) {
            if ( ( signum == SIGTERM ) || ( signum == SIGQUIT ) || ( signum == SIGINT ) ) {
                app.quit();
            }
        }
    );

    if ( not app.lockApplication() ) {
        return 0;
    }

    app.setQuitOnLastWindowClosed( false );

    powerSett = DesQ::Utils::initializeDesQSettings( "Power", "Power" );

    DesQ::Power::Manager *pm = new DesQ::Power::Manager();
    pm->showTrayIcon();

    return app.exec();
}
